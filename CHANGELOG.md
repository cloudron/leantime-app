[0.1.0]
* Initial version for Leantime v2.3.27

[0.2.0]
* Add cron job runner every 5min
* Fix uploading of company logo

[0.3.0]
* Remove redis config because there appears to be a memory leak - https://github.com/Leantime/leantime/issues/1780

[0.4.0]
* Ensure custom settings from /app/data/env

[0.5.0]
* Update leantime to 2.4.0
* Update base image to 4.2
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4)

[0.6.0]
* Update leantime to 2.4.1
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4.1)
* Mark down support by @marcelfolaron in #1973
* Fix Broken Links in CONTRIBUTING.md by @chriscavalluzzi in #1970

[1.0.0]
* First stable package release for Leantime 2.4.2
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4.2)
* Bug fixes by @marcelfolaron in https://github.com/Leantime/leantime/pull/1982
* fix: anonymous component error
* fix: Wrong type if user by sha cannot be found (ical)
* fix: Check if menu type is set (empty if no project)
* Remove unnecessary logs
* fix: Check if session is set since in cases the data comes via ical
* fix: remove double slash to avoid class duplication bug
* fix: newticket model was not initialized correctly
* fix: remove milestone issues on several canvas dialogs
* fix(css): menu resizing issue on smaller devices

[1.0.1]
* Update Leantime to 2.4.3
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4.3)
* Fix: canvas repository not found on blueprints by @marcelfolaron in https://github.com/Leantime/leantime/pull/1990
* Remove typos by @omimakhare in https://github.com/Leantime/leantime/pull/1998
* Fix List icons in German Language by @Stiles96 in https://github.com/Leantime/leantime/pull/1996

[1.0.2]
* Update Leantime to 2.4.4
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4.4)
* fix: variety of bug fixes by @marcelfolaron in #2012
* fix: Firefox can't handle the animations. Removing by @marcelfolaron in #2019
* fix: milestone progress calculation not correct by @marcelfolaron in #2020

[1.0.3]
* Update Leantime to 2.4.5
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4.5)
* fix: content overflow with long project names #2021 by @marcelfolaron in https://github.com/Leantime/leantime/pull/2022
* Configure Sweep by @sweep-ai in https://github.com/Leantime/leantime/pull/2026
* Refactor telemetry handling by @marcelfolaron in https://github.com/Leantime/leantime/pull/2029
* feat(login form): hide form if config is true by @eric-hansen in https://github.com/Leantime/leantime/pull/2025
* Update README.md by @bhargavshirin in https://github.com/Leantime/leantime/pull/2037
* [Snyk] Upgrade datatables.net-rowgroup from 1.4.0 to 1.4.1 by @broskees in https://github.com/Leantime/leantime/pull/2041
* Fixes from production by @marcelfolaron in https://github.com/Leantime/leantime/pull/2039
* Fix OIDC by @marcelfolaron in https://github.com/Leantime/leantime/pull/2048
* Getting started links by @marcelfolaron in https://github.com/Leantime/leantime/pull/2049
* Fixed typos in README.md by @vaibhav-009 in https://github.com/Leantime/leantime/pull/2050
* Update .gitignore by @marcelfolaron in https://github.com/Leantime/leantime/pull/2053
* Align db versions with actual version number by @marcelfolaron in https://github.com/Leantime/leantime/pull/2054

[1.1.0]
* Update Leantime to 2.4.7
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4.7)
* Fix plugin management

[1.1.1]
* Update Leantime to 2.4.8
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v2.4.8)
* fix: changed logtime endpoint form using $_POST to using $params data by @jeppekroghitk in https://github.com/Leantime/leantime/pull/2074
* [Snyk] Upgrade uppy from 3.17.0 to 3.18.0 by @broskees in https://github.com/Leantime/leantime/pull/2079
* Adds IIS-specific installation note to README.md by @MeatyFresh in https://github.com/Leantime/leantime/pull/2090
* [Snyk] Upgrade uppy from 3.18.0 to 3.18.1 by @broskees in https://github.com/Leantime/leantime/pull/2089
* [Snyk] Security upgrade tinymce from 5.10.8 to 5.10.9 by @broskees in https://github.com/Leantime/leantime/pull/2096
* Fix OIDC last name and phone by @fabiorino in https://github.com/Leantime/leantime/pull/2097
* Improved and finished Spanish translation by @jontorrado in https://github.com/Leantime/leantime/pull/2094
* bugfixes and updates discovered in prod by @marcelfolaron in https://github.com/Leantime/leantime/pull/2100
* Added default value to getAllSprints by @jeppekroghitk in https://github.com/Leantime/leantime/pull/2111
* Fixes language error by @jontorrado in https://github.com/Leantime/leantime/pull/2109
* Improved Slack and Mattermost notifications by @jontorrado in https://github.com/Leantime/leantime/pull/2107
* Added missing Spanish translations for 2.4.4 and 2.4.5 by @jontorrado in https://github.com/Leantime/leantime/pull/2104
* Added danish translation by @jeppekroghitk in https://github.com/Leantime/leantime/pull/2113
* Added method for determining API context by @jeppekroghitk in https://github.com/Leantime/leantime/pull/2115
* [Snyk] Upgrade htmx.org from 1.9.6 to 1.9.7 by @broskees in https://github.com/Leantime/leantime/pull/2117
* [Snyk] Upgrade datatables.net from 1.13.6 to 1.13.7 by @broskees in https://github.com/Leantime/leantime/pull/2116
* Added Danish, Slovenian and Khmer to language list by @marcelfolaron in https://github.com/Leantime/leantime/pull/2118
* Add Korean language support by @williamjeong2 in https://github.com/Leantime/leantime/pull/2119
* [Snyk] Upgrade htmx.org from 1.9.7 to 1.9.8 by @broskees in https://github.com/Leantime/leantime/pull/2121
* [Snyk] Upgrade uppy from 3.18.1 to 3.19.0 by @broskees in https://github.com/Leantime/leantime/pull/2125
* Bug fixes 020408 by @marcelfolaron in https://github.com/Leantime/leantime/pull/2126
* prod bug fixes by @marcelfolaron in https://github.com/Leantime/leantime/pull/2127

[1.1.1-1]
* Fix company logo upload

[1.2.0]
* Update Leantime to 3.0.2
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.0.0)

[1.2.1]
* Update Leantime to 3.0.3
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.0.3)
* Update README.md - LEAN_APP_URL by @TheBilltrain in https://github.com/Leantime/leantime/pull/2230
* changes needed for custom fields by @broskees in https://github.com/Leantime/leantime/pull/2239
* Translations updates by @jontorrado in https://github.com/Leantime/leantime/pull/2237
* Fix cropped textbox content effected by rounder textbox border. by @bankzst in https://github.com/Leantime/leantime/pull/2241
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2240
* Fix: Added autofocus to 2fa verify HTML field by @cableman in https://github.com/Leantime/leantime/pull/2246
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2248
* misc fixes by @marcelfolaron in https://github.com/Leantime/leantime/pull/2249

[1.2.2]
* Update Leantime to 3.0.4
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.0.4)
* Added milestone and tags to all timesheet view by @cableman in https://github.com/Leantime/leantime/pull/2267
* Plugin settings minor markup fix by @cableman in https://github.com/Leantime/leantime/pull/2268
* Updated code style for commands by @cableman in https://github.com/Leantime/leantime/pull/2269
* Updated code style for helper and views by @cableman in https://github.com/Leantime/leantime/pull/2270
* Updated code style for API classes by @cableman in https://github.com/Leantime/leantime/pull/2271
* Image fixes by @marcelfolaron in https://github.com/Leantime/leantime/pull/2274
* quote fixes across all language files by @marcelfolaron in https://github.com/Leantime/leantime/pull/2275
* versionbump by @marcelfolaron in https://github.com/Leantime/leantime/pull/2276

[1.2.3]
* Update Leantime to 3.0.6
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.0.6)
* Timesheet table format fixed
* Timesheet descriptions for the same day and billing type now get concatenated
* Security bug fixes
* Download file issue when using OIDC
* A few German translations added. by @Elaktrato in https://github.com/Leantime/leantime/pull/2290
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2292
* Fix milestone dates not saved and theme settings not saved by @marcelfolaron in https://github.com/Leantime/leantime/pull/2294
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2295
* Promote plugins by @marcelfolaron in https://github.com/Leantime/leantime/pull/2296
* Fixing Subtask Link not opening on same page by @marcelfolaron in https://github.com/Leantime/leantime/pull/2297
* fix error on empty status by @marcelfolaron in https://github.com/Leantime/leantime/pull/2298

[1.2.4]
* Update Leantime to 3.0.7
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.0.7)
* pad strings to ensure timeline sorting is correct by @marcelfolaron in https://github.com/Leantime/leantime/pull/2301
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2299
* Translations for formal and informal German by @Elaktrato in https://github.com/Leantime/leantime/pull/2302
* Reset invite code if not set by @marcelfolaron in https://github.com/Leantime/leantime/pull/2303
* Improve project hub, speed improvals, fix favorite flow by @marcelfolaron in https://github.com/Leantime/leantime/pull/2305
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2304
* Fixes and improvements to project hub by @marcelfolaron in https://github.com/Leantime/leantime/pull/2307
* Project hub updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2308
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2306
* Update Cast.php by @marcelfolaron in https://github.com/Leantime/leantime/pull/2317
* Filter for notifications by @marcelfolaron in https://github.com/Leantime/leantime/pull/2318
* Timesheet issue by @marcelfolaron in https://github.com/Leantime/leantime/pull/2319
* Updated code style for API classes - part II by @cableman in https://github.com/Leantime/leantime/pull/2325
* Updated code style for calendar namespace by @cableman in https://github.com/Leantime/leantime/pull/2327
* Added PR template for easier review process by @cableman in https://github.com/Leantime/leantime/pull/2328
* fix timezone issues on timesheet management by @marcelfolaron in https://github.com/Leantime/leantime/pull/2341
* Ical fix by @marcelfolaron in https://github.com/Leantime/leantime/pull/2344
* Moved all testing into docker containers by @cableman in https://github.com/Leantime/leantime/pull/2340
* Delete old screenshots by @marcelfolaron in https://github.com/Leantime/leantime/pull/2345
* Enable View Transitions for smooth transitions between page loads by @marcelfolaron in https://github.com/Leantime/leantime/pull/2347
* Updated code style for audit namespace by @cableman in https://github.com/Leantime/leantime/pull/2326
* [Snyk] Upgrade gridstack from 10.0.1 to 10.1.0 by @broskees in https://github.com/Leantime/leantime/pull/2348
* Onboarding improvements by @marcelfolaron in https://github.com/Leantime/leantime/pull/2349
* New Crowdin updates by @marcelfolaron in https://github.com/Leantime/leantime/pull/2350
* Misc fixes by @marcelfolaron in https://github.com/Leantime/leantime/pull/2351

[1.3.0]
* Update Leantime to 3.1.1
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.1.1)
* Update CONTRIBUTING.md by @marcelfolaron in #2422
* [Snyk] Upgrade @sentry/browser from 7.102.1 to 7.103.0 by @broskees in #2423
* [Snyk] Upgrade datatables.net from 1.13.10 to 1.13.11 by @broskees in #2424
* [Snyk] Upgrade uppy from 3.22.2 to 3.23.0 by @broskees in #2425
* Update zh-CN.ini by @rqi14 in #2429
* Fix content-disposition header by @gramakri in #2431
* New Crowdin updates by @marcelfolaron in #2433
* New Crowdin updates by @marcelfolaron in #2435
* [Snyk] Upgrade @sentry/browser from 7.103.0 to 7.104.0 by @broskees in #2436
* [Snyk] Upgrade gridstack from 10.1.0 to 10.1.1 by @broskees in #2437
* Misc ux fixes by @marcelfolaron in #2434
* [Snyk] Upgrade @lottiefiles/lottie-player from 2.0.3 to 2.0.4 by @broskees in #2440
* New Crowdin updates by @marcelfolaron in #2438
* Phpstan lvl0 by @marcelfolaron in #2450
* Castupdates by @marcelfolaron in #2451
* Update Notifications.php by @marcelfolaron in #2452

[1.3.1]
* Update Leantime to 3.1.2
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.1.2)
* fix incorrect symbol in the translation for zh-CN by @rqi14 in https://github.com/Leantime/leantime/pull/2461
* Fixes projectCalendar and comment dates. by @marcelfolaron in https://github.com/Leantime/leantime/pull/2463
* Middle wareimprovements by @marcelfolaron in https://github.com/Leantime/leantime/pull/2475
* [Changelog CI] Add Changelog for Version 3.1.1 by @github-actions in https://github.com/Leantime/leantime/pull/2480
* [Snyk] Upgrade gridstack from 10.1.1 to 10.1.2 by @broskees in https://github.com/Leantime/leantime/pull/2476
* [Snyk] Upgrade uppy from 3.23.0 to 3.24.0 by @broskees in https://github.com/Leantime/leantime/pull/2471
* [Snyk] Upgrade htmx.org from 1.9.10 to 1.9.11 by @broskees in https://github.com/Leantime/leantime/pull/2469
* [Snyk] Upgrade @sentry/webpack-plugin from 2.14.2 to 2.16.0 by @broskees in https://github.com/Leantime/leantime/pull/2468
* [Snyk] Upgrade @sentry/browser from 7.104.0 to 7.108.0 by @broskees in https://github.com/Leantime/leantime/pull/2467
* [Snyk] Upgrade @sentry/browser from 7.108.0 to 7.109.0 by @broskees in https://github.com/Leantime/leantime/pull/2481
* Milestone project selector fix by @marcelfolaron in https://github.com/Leantime/leantime/pull/2483
* Add project return by @marcelfolaron in https://github.com/Leantime/leantime/pull/2484
* Hide company menu point by @marcelfolaron in https://github.com/Leantime/leantime/pull/2485

[1.3.2]
* Update Leantime to 3.1.3
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.1.3)
* [Changelog CI] Add Changelog for Version 3.1.2 by @github-actions in https://github.com/Leantime/leantime/pull/2486
* Default theme fixes by @marcelfolaron in https://github.com/Leantime/leantime/pull/2491
* [Snyk] Upgrade @fortawesome/fontawesome-free from 6.5.1 to 6.5.2 by @broskees in https://github.com/Leantime/leantime/pull/2489
* [Snyk] Upgrade @sentry/webpack-plugin from 2.16.0 to 2.16.1 by @broskees in https://github.com/Leantime/leantime/pull/2490
* [fix] Delete user should remove project relationships by @marcelfolaron in https://github.com/Leantime/leantime/pull/2493
* Improve onboarding flow by @marcelfolaron in https://github.com/Leantime/leantime/pull/2492

[1.3.3]
* Update Leantime to 3.1.4
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.1.4)
* c3bdd04: [fix] news notification doesn't disappear bug
* ef6a6c6: [fix] news notification doesn't disappear bug
* fc6cf2f: [enhancement] Align initial filters on timesheet list to be the current week
* ab40097: [fix] disabling adding time registrations in previous timezone
* 9f9adf0: clean up.
* 2e9db31: [fix] timesheet entries that fall on midnight were causing 0 hour overwrites

[1.4.0]
* Update Leantime to 3.2.0
* [Full changelog](https://github.com/Leantime/leantime/releases/tag/v3.2.0)

[1.5.0]
* Update leantime to 3.3.1
* [Full Changelog](https://github.com/Leantime/leantime/releases/tag/v3.2.0)
* [9285883](https://github.com/Leantime/leantime/commit/9285883d0c950d25add07dd6a5aa7d0bed421ee2): Update StringableMacros.php
* [138de0e](https://github.com/Leantime/leantime/commit/138de0e8190f9835cb4941495ff34776097af32d): update comments for phpstan
* [671794b](https://github.com/Leantime/leantime/commit/671794ba695331b245a40460f5ad0e1ca6029790): mixin updates
* [ab23d11](https://github.com/Leantime/leantime/commit/ab23d119581765559d6fd909b7fd652deb02212d): Adding comments for phpstan
* [4eabdb1](https://github.com/Leantime/leantime/commit/4eabdb19e840ebc49a27be99b2734525fdda149c): Resolving open basedir restrictions with font files in avatar generation scripts
* [c4032e0](https://github.com/Leantime/leantime/commit/c4032e009751aba406e331ce18234a33cc212d9a): Version bump
* [1875f07](https://github.com/Leantime/leantime/commit/1875f07c912e4e1d5f36499ad709c6b8beebb756): code style fixes
* [8ad64a6](https://github.com/Leantime/leantime/commit/8ad64a64c0b65b12d41c1091ff420ec4a284a717): Update .gitignore
* [99357d0](https://github.com/Leantime/leantime/commit/99357d001372721773a3614d3a122c2ce9717299): Deprecate configuration.php file and create backward support
* ...

[1.5.1]
* Update leantime to 3.3.2
* [Full Changelog](https://github.com/Leantime/leantime/releases/tag/v3.2.0)
* [4a952c8](https://github.com/Leantime/leantime/commit/4a952c8b521fa9db7b7b04d968102521310b0440): Update README.md
* [d5d1c8f](https://github.com/Leantime/leantime/commit/d5d1c8f90f66fbff397727757ac1b377a6a49140): Version bump
* [034a158](https://github.com/Leantime/leantime/commit/034a1581bb04babc54595cfb46d8ef56146ec599): attribute class
* [7e2ea50](https://github.com/Leantime/leantime/commit/7e2ea500500f047460d70747a569b1cd0b756fcd): fix csv importer
* [2b11141](https://github.com/Leantime/leantime/commit/2b1114191776313746f252a243b96f25177793f4): Update Projects.php
* [4f0998f](https://github.com/Leantime/leantime/commit/4f0998f651684417657e5bef6daefa2432078136): Update de-DE.ini
* [31d54ce](https://github.com/Leantime/leantime/commit/31d54cebf18c9a3204cb612e7c30f14a6a73dd74): fix code styles
* [8cb7914](https://github.com/Leantime/leantime/commit/8cb79140d77dcc6c2c13e8e928b4e1dfabea6168): Clean up tests
* [39d18c1](https://github.com/Leantime/leantime/commit/39d18c1e9e61e3a57a71174c6d30071969243b9c): Update UnitTesterActions.php
* [5f1b0d8](https://github.com/Leantime/leantime/commit/5f1b0d8a6fd0e3094d0c43737c0d8b02ff577192): Unit tests for jsonrpc controller
* [fc40528](https://github.com/Leantime/leantime/commit/fc40528efcd64ffa387f5dc1dfea6e3ce554fdb3): Fix code style
* [4b78fdd](https://github.com/Leantime/leantime/commit/4b78fdde3a6e9bfaeb17ba245df24e690f69906b): update readme and test location
* [ec63cdc](https://github.com/Leantime/leantime/commit/ec63cdc2bf49b093c04855d49f05e86501ce65c6): Update en-US.ini
* [a5c9ff8](https://github.com/Leantime/leantime/commit/a5c9ff8a3cda237ad9186c654d6b5219cb3dabf9): Fix API calls and write unit tests
* [aebcf67](https://github.com/Leantime/leantime/commit/aebcf67ecc331adc29d3c0f19bae4ce3acba7f42): Fixes issue where milestones without end dates are causing 500 error
* [cd3cf13](https://github.com/Leantime/leantime/commit/cd3cf13d4e78b0971ee47e71f2e949c92569bb43): delete Pirate.json
* [6cffcf6](https://github.com/Leantime/leantime/commit/6cffcf6d67349e3632a00894a829fe724dd1c6db): Fixes wrong dates in milestone calendar header

[1.5.2]
* Update leantime to 3.3.3
* [Full Changelog](https://github.com/Leantime/leantime/releases/tag/v3.2.0)
* [752bfec](https://github.com/Leantime/leantime/commit/752bfec2650b4da9cd5c2310b8ae4ea3901a27b8): Version Bump
* [f5717e6](https://github.com/Leantime/leantime/commit/f5717e6c9c1d5ce815251f9e1f3676876e040b68): Merge remote-tracking branch 'origin/master'
* [f44dbb8](https://github.com/Leantime/leantime/commit/f44dbb8fc73f56fa6082089a21d92da26c46cead): style fixes

[1.6.0]
* Update leantime to 3.4.0
* [Full Changelog](https://github.com/Leantime/leantime/releases/tag/v3.2.0)
* [31f2ce7](https://github.com/Leantime/leantime/commit/31f2ce78a023bbcae9a8213c78c57487ea0b4332): fix user issue
* [7931979](https://github.com/Leantime/leantime/commit/7931979511693f4fda83906344c1d1430b025b3e): bugfix for project duplication
* [20c4ba6](https://github.com/Leantime/leantime/commit/20c4ba64cb8568d726c6a16494a020decf94a46f): Escaping content for email messages
* [5fdb464](https://github.com/Leantime/leantime/commit/5fdb46467ecbb3c8a9f78f86cc56a9feaf2a116c): fixes unescaped string
* [2a5045f](https://github.com/Leantime/leantime/commit/2a5045f6182abdce02f798e47d88904d24716b5b): Improved cache control
* [759e1e3](https://github.com/Leantime/leantime/commit/759e1e39b8a0ac6bcb675c52b3d3d8a1656d90dd): Plugin page improvements
* [14df6b2](https://github.com/Leantime/leantime/commit/14df6b25764203ca107c5acf4726ede98e08fa5f): Update source file en-US.ini
* [d056731](https://github.com/Leantime/leantime/commit/d0567316d1987079dbb13587515e2de66a867dec): endpoint to get current leantime version [#&#8203;1152](https://github.com/Leantime/leantime/issues/1152)
* [d9e9a17](https://github.com/Leantime/leantime/commit/d9e9a174361587d4c01d7d3c1566d2d2e4b154ed): Fix broken timer on ticketmodal [#&#8203;2849](https://github.com/Leantime/leantime/issues/2849)
* [9fa14c2](https://github.com/Leantime/leantime/commit/9fa14c28f5a62b3ee2d31ea61670016d1e213f6b): Fix broken timer on ticketmodal [#&#8203;2845](https://github.com/Leantime/leantime/issues/2845)
* [a805662](https://github.com/Leantime/leantime/commit/a805662639e04b12f934a2c41b1182f69e4c8417): ticket table pager issue
* [005f212](https://github.com/Leantime/leantime/commit/005f2125a17fa356e504d6e41fabd457832dae13): New translations en-us.ini (German (Informal))

[1.6.1]
* Update leantime to 3.4.1
* [Full Changelog](https://github.com/Leantime/leantime/releases/tag/v3.4.1)
* [66271fa](https://github.com/Leantime/leantime/commit/66271faa0b9fb6a15614951b4b0dc43100c2e019): task: add manifest and package-lock
* [9434188](https://github.com/Leantime/leantime/commit/943418870c9387a59b5e07aeca3de20786185d86): feat: Update Release Version to 3.4.1
* [db913f8](https://github.com/Leantime/leantime/commit/db913f8f815e134139c5292d110e43f56e81359e): Update release.yml
* [606be4d](https://github.com/Leantime/leantime/commit/606be4d25702318bcbfc4206daa2cb209331e2aa): fix: various dark theme bugs and saving theme settings issues
* [43635f9](https://github.com/Leantime/leantime/commit/43635f913d06fea150b983599242eda6d1dc1386): fix: Show tasks on milestone page without task parents shows 500
* [3f3a389](https://github.com/Leantime/leantime/commit/3f3a3899744d70b470cd2800c718b8a21c48de0f): Update SECURITY.md
* [435e3f7](https://github.com/Leantime/leantime/commit/435e3f744d808d1d38497b9f1dcad2b8d5a1c14b): Update SECURITY.md
* [f566455](https://github.com/Leantime/leantime/commit/f5664556a3595225d84a23686f21b0d9dc3d482b): Update SECURITY.md
* [e9924fb](https://github.com/Leantime/leantime/commit/e9924fbe07e6defebc8242fe613152ba68cc930d): Readme updates
* [0f9a831](https://github.com/Leantime/leantime/commit/0f9a8310e7d8005b31c53ee2622625cfb9dfdb1d): Additional Readme Updates
* [85cc204](https://github.com/Leantime/leantime/commit/85cc204e658c24f1fd1b1bc0b0aa78faab0bf900): Readme updates and new screenshots
* [a7ca7eb](https://github.com/Leantime/leantime/commit/a7ca7ebafd4f88014d138649809c6a00ad7cd38c): Update bug_report.yml
* [bc28a1b](https://github.com/Leantime/leantime/commit/bc28a1bea2c0c7f3522b6982e7f18c6e61936337): Update feature_request.yml
* [bf1b1f0](https://github.com/Leantime/leantime/commit/bf1b1f07125cc35d182a16111ea67b3bfb4fd439): Update feature_request.yml
* [76c3df1](https://github.com/Leantime/leantime/commit/76c3df1469ccebc9f6d43681de34707253b6545f): Update feature_request.yml
* [a3261e9](https://github.com/Leantime/leantime/commit/a3261e9fab2146f51e1167779d255f007137f648): Update bug_report.yml
* [02d0ac7](https://github.com/Leantime/leantime/commit/02d0ac72b62d5a97ef9977be4b9331638ee3f0cd): Update bug_report.yml

[1.6.2]
* Update leantime to 3.4.2
* [Full Changelog](https://github.com/Leantime/leantime/releases/tag/v3.4.2)
* [14e71ad](https://github.com/Leantime/leantime/commit/14e71adada985e1c1f759cebde46d68da4301eb7): fix: typo in provider name
* [268ae34](https://github.com/Leantime/leantime/commit/268ae349570bd87fe6d823382633a503a6dc273a): Update DateTimeHelper.php
* [e9d59a0](https://github.com/Leantime/leantime/commit/e9d59a05dc96556b963f48d8edf84a2438265d33): fix: update and expand standard iso8601 dateformat management
* [c7964a1](https://github.com/Leantime/leantime/commit/c7964a17feec73f782d8046a965918fa4514bba1): fix: datetime issues in calendar and sprints
* [a9ec032](https://github.com/Leantime/leantime/commit/a9ec03242489c597e3bc8f448709f938d38ba24b): fix: dashboard delete status and allow replies
* [be8ee46](https://github.com/Leantime/leantime/commit/be8ee466b42f1bf9c41f0b0f85f7c25c6f3e44bf): task: fix code style and update version number
* [9ed32f9](https://github.com/Leantime/leantime/commit/9ed32f9d140da2a68170c59dfb9059bfc8196bcc): Update Plugins
* [ca1a2f6](https://github.com/Leantime/leantime/commit/ca1a2f6a37307297f033e5c559a7e2acce1b80e7): fix: Ensure milestone hierarchy is loaded and displayed correctly
* [abd7343](https://github.com/Leantime/leantime/commit/abd734333fd3655ed95374465c1036b3268a9676): feat: various small design improvements
* [fd4a040](https://github.com/Leantime/leantime/commit/fd4a040b1bbdfa285222f6ef2438a31a6708adcf): fix: theme image could not be loaded due to test chars in header
* [241303b](https://github.com/Leantime/leantime/commit/241303b31652f36c6b51f57efaf740ea3b42f608): feat: add new template event for ticket due date
* [8451caf](https://github.com/Leantime/leantime/commit/8451caf3d903421ff49797ebf130c0bd35e43cf7): fix: formatting for status field in subtasks
* [04169c5](https://github.com/Leantime/leantime/commit/04169c50c6deb005e96e3307e39b81a161022350): fix: import project via csv import failed due to missing state
* [3e35f56](https://github.com/Leantime/leantime/commit/3e35f568939d1f320f59a2f2e7e9280e34788d12): feat: preload important urls on mouseover
* [af056a9](https://github.com/Leantime/leantime/commit/af056a9598feb58ec6172860fb1f1438f93540f4): fix: name in testemail command
* [c1bd5c4](https://github.com/Leantime/leantime/commit/c1bd5c467d7f5fae46fd10ec71870ea4a31760a7): feat: Ensure images and other static files are cached by browsers
* [65fe92e](https://github.com/Leantime/leantime/commit/65fe92e6323139c76930c3473bb43149fe47355a): feat: Improved error reporting for json parse errors on api requests
* [8d33277](https://github.com/Leantime/leantime/commit/8d332775e3935b1db7116c904936521adcbaac3f): fix: Allow usernames with spaces (ldap issue)
* [9ebf1ea](https://github.com/Leantime/leantime/commit/9ebf1ea68b2fe16c5459ee1d72028e53ef092e0f): fix: i18n file for js was not loaded correctly with plugin language files
* [15de91e](https://github.com/Leantime/leantime/commit/15de91e474679613f45db34caff44939e2781cde): feat: new command to clear language cache files

[1.6.3]
* Update leantime to 3.4.3
* [Full Changelog](https://github.com/Leantime/leantime/releases/tag/v3.4.3)
* [af62ea4](https://github.com/Leantime/leantime/commit/af62ea4e6e467240a180a0fa073c127ef6c212b4): task: clean up ticket service
* [deffc4e](https://github.com/Leantime/leantime/commit/deffc4e855682ba28eeaa846d92d7d84708a6c2e): fix: not sending notifications when project id is not set correctly
* [e5384f2](https://github.com/Leantime/leantime/commit/e5384f22a9dd0b3ccc3a05f233d754b25fcd7a2e): fix: console kernel to point to the correct binary
* [12ad2e5](https://github.com/Leantime/leantime/commit/12ad2e501945ecc27a96cab5d1df31dd511fcc9b): fix api endpoint to get open user tickets
* [7b3539d](https://github.com/Leantime/leantime/commit/7b3539d94071df590cf20e5724a81d2f2aa06ee3): task: update version
* [2759494](https://github.com/Leantime/leantime/commit/2759494d2cd55a4c727abb8718ee866ceffb8637): task: Improve dailyIngestion report management
* [8a3ee6a](https://github.com/Leantime/leantime/commit/8a3ee6a1cfd918f49c5ed396584043f5a4c46ce0): fix: add missing field
* [1d0b242](https://github.com/Leantime/leantime/commit/1d0b242917b9dcd0ee92cc0bdc2ccc2fef5fcaa4): fix: broken datetime management on sprints creating various bugs throughout the system
* [86b40bd](https://github.com/Leantime/leantime/commit/86b40bdbbf87c814204c24dd974b77d1767e2ef1): task: Cleanup and improve datetime helper class
* [3dd728b](https://github.com/Leantime/leantime/commit/3dd728b5770dad0bdf5e97e3fec92efd671d5bed): Fix link

