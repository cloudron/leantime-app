### Overview

Leantime is a strategic open source project management system for innovative companies and teams looking to go from start to finish.
Built for the non-project manager, we combine the plans and the work while making it easy for everyone on the team to use.
It's an alternative to ClickUp, Notion, and Asana. As simple as Trello but as feature rich as Jira.
