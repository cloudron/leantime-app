FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg

# renovate: datasource=github-releases depName=Leantime/leantime versioning=semver extractVersion=^v(?<version>.+)$
ARG LEANTIME_VERSION=3.4.3

RUN curl -L https://github.com/Leantime/leantime/releases/download/v${LEANTIME_VERSION}/Leantime-v${LEANTIME_VERSION}.tar.gz | tar -xz --strip-components=1 -C /app/code

RUN rm -rf /app/code/{logs,cache,userfiles,public/userfiles,app/custom,app/Plugins} && \
    ln -sf /app/data/plugins /app/code/app/Plugins && \
    # rm -rf /app/code/storage && ln -sf /run/leantime/storage /app/code/storage && \
    # rm -rf /app/code/bootstrap/cache && ln -sf /run/leantime/bootstrap-cache /app/code/bootstrap/cache && \
    ln -sf /run/leantime/env /app/code/config/.env && \
    ln -sf /app/data/leantime/userfiles /app/code/userfiles && \
    ln -sf /app/data/leantime/public/userfiles /app/code/public/userfiles && \
    ln -sf /app/data/leantime/app/custom /app/code/app/custom && \
    ln -sf /app/data/leantime/backupdb /app/code/backupdb && \
    ln -sf /run/snmp /var/lib/snmp && \
    chown -R www-data:www-data /app/code

# configure apache
RUN a2disconf other-vhosts-access-log && \
    echo "Listen 8080" > /etc/apache2/ports.conf && \
    a2enmod rewrite env && \
    a2dismod perl && \
    rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf

COPY apache/leantime.conf /etc/apache2/sites-enabled/leantime.conf

RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_execution_time 120 && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP opcache.enable 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP opcache.jit 1255 && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP opcache.jit_buffer_size 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/leantime/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_maxlifetime 604800

RUN ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

WORKDIR /app/code

COPY start.sh cron.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
