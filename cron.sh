#!/bin/bash

set -eu

echo "=> Running cron jobs"

# this is broken: https://github.com/Leantime/leantime/issues/2228
# https://docs.leantime.io/#/installation/configuration?id=cron-jobs
# /app/code/bin/leantime cron:run

curl https://${CLOUDRON_APP_DOMAIN}/cron/run
