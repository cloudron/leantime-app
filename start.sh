#!/bin/bash

set -eu

echo "=> Creating directories"
mkdir -p /app/data/leantime/{userfiles,backupdb} \
         /app/data/leantime/cache/avatars \
         /app/data/leantime/app/{custom,plugins} \
         /app/data/leantime/public/userfiles \
         /app/data/plugins \
         /app/code/storage/framework/sessions \
         /run/leantime \
         /run/snmp

if [[ ! -f /app/data/.sessions_secret ]]; then
    echo "=> Create SESSIONS secret"
    openssl rand -base64 32 > /app/data/.sessions_secret
fi
LEAN_SESSION_PASSWORD=$(cat /app/data/.sessions_secret)

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/env ]]; then
    echo "=> Create custom env file template"
    echo -e "# See all config options at https://github.com/Leantime/leantime/blob/master/config/sample.env\n\n" > /app/data/env
fi

echo "=> Ensure Cloudron related env variables"
cp /app/data/env /run/leantime/env
cat >> /run/leantime/env <<EOT

LEAN_APP_URL = '${CLOUDRON_APP_ORIGIN}'
LEAN_APP_DIR = ''

LEAN_SESSION_PASSWORD = '${LEAN_SESSION_PASSWORD}'

LEAN_DB_HOST = '${CLOUDRON_MYSQL_HOST}'
LEAN_DB_USER = '${CLOUDRON_MYSQL_USERNAME}'
LEAN_DB_PASSWORD = '${CLOUDRON_MYSQL_PASSWORD}'
LEAN_DB_DATABASE = '${CLOUDRON_MYSQL_DATABASE}'
LEAN_DB_PORT = '${CLOUDRON_MYSQL_PORT}'

LEAN_EMAIL_RETURN = '${CLOUDRON_MAIL_FROM}'
LEAN_EMAIL_USE_SMTP = true
LEAN_EMAIL_SMTP_HOSTS = '${CLOUDRON_MAIL_SMTP_SERVER}'
LEAN_EMAIL_SMTP_AUTH = true
LEAN_EMAIL_SMTP_USERNAME = '${CLOUDRON_MAIL_SMTP_USERNAME}'
LEAN_EMAIL_SMTP_PASSWORD = '${CLOUDRON_MAIL_SMTP_PASSWORD}'
LEAN_EMAIL_SMTP_AUTO_TLS = false
LEAN_EMAIL_SMTP_SECURE = ''
LEAN_EMAIL_SMTP_SSLNOVERIFY = false
LEAN_EMAIL_SMTP_PORT = '${CLOUDRON_MAIL_SMTP_PORT}'

EOT

if [[ ! -f /app/data/.db_initialised ]]; then
    echo "=> DB init"
    /app/code/bin/leantime db:migrate --email='admin@cloudron.local' --password='changeme' --first-name='Admin' --last-name='Admin' --company-name='Leantime'
    touch /app/data/.db_initialised
fi

echo "=> DB migration"
/app/code/bin/leantime db:migrate 

echo "=> Changing ownership"
chown -R www-data:www-data /app/data /run/leantime /app/code/storage

echo "=> Starting Leantime"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
