#!/usr/bin/env node

'use strict';

/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_USERNAME='admin@cloudron.local';
    const ADMIN_PASSWORD='changeme';
    const LOCATION = process.env.LOCATION || 'test';
    const PROJECT_NAME = 'test project cloudron ' + Math.floor((Math.random() * 100) + 1);
    const TASK_NAME = 'test task cloudron' + Math.floor((Math.random() * 100) + 1);
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password, firstTime = false) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/auth/login');
        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//input[@value="Login"]')).click();
        await browser.sleep(3000);

        if (firstTime) {
            await browser.findElement(By.id('projectName')).sendKeys(`First ${PROJECT_NAME}`);
            await waitForElement(By.xpath('//input[@type="submit" and @value="Next"]'));
            await browser.findElement(By.xpath('//input[@type="submit" and @value="Next"]')).click();

            // await waitForElement(By.id('accomplish'));
            // await waitForElement(By.xpath('//input[@type="submit" and @value="Next"]'));
            // await browser.findElement(By.xpath('//input[@type="submit" and @value="Next"]')).click();

            // await waitForElement(By.xpath('//h1[contains(text(), "Invite the crew")]'));
            // await waitForElement(By.xpath('//a[contains(text(), "Skip for now")]'));
            // await browser.findElement(By.xpath('//a[contains(text(), "Skip for now")]')).click();
        }

        await waitForElement(By.xpath('//div[@class="userloggedinfo"]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/auth/logout');
        await browser.sleep(3000);
        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//input[@value="Login"]'));
    }

    async function contentExists() {
        await browser.get('https://' + app.fqdn + '/projects/showAll');
        await browser.sleep(5000);

        if (await browser.findElements(By.xpath('//a[.="Close and don\'t show this screen again"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//a[.="Close and don\'t show this screen again"]')).click();
        }

        await waitForElement(By.xpath(`//table[@id="allProjectsTable"]//td//a[text()="${PROJECT_NAME}"]`));

        await browser.findElement(By.xpath(`//table[@id="allProjectsTable"]//td//a[text()="${PROJECT_NAME}"]`)).click();
        await browser.sleep(2000);

        await browser.get('https://' + app.fqdn + '/tickets/showKanban');
        await browser.sleep(2000);
        await waitForElement(By.xpath(`//div[@class="kanbanCardContent"]//h4/a[contains(., "${TASK_NAME}")]`));
    }

    async function createProject() {
        await browser.get(`https://${app.fqdn}/projects/newProject/`);

        await browser.findElement(By.id('name')).sendKeys(`${PROJECT_NAME}`);
        await browser.findElement(By.xpath('//input[@value="Save"]')).click();

        await browser.sleep(5000);
        await browser.get('https://' + app.fqdn + '/projects/showAll');
        await waitForElement(By.xpath(`//table[@id="allProjectsTable"]//td//a[text()="${PROJECT_NAME}"]`));
    }

    async function addTask() {
        await browser.get('https://' + app.fqdn + '/tickets/showKanban');

        await waitForElement(By.xpath('//button[contains(@class, "dropdown-toggle") and contains(.,"New")]'));
        await browser.findElement(By.xpath('//button[contains(@class, "dropdown-toggle") and contains(.,"New")]')).click();
        await browser.findElement(By.xpath('//a[@class="ticketModal" and contains(., "Add To-Do")]')).click();
        await waitForElement(By.xpath('//input[@name="headline" and @class="main-title-input"]'));
        await browser.findElement(By.xpath('//input[@name="headline" and @class="main-title-input"]')).sendKeys(TASK_NAME);
        await browser.findElement(By.id('tags_tag')).sendKeys(TASK_NAME);
        await browser.findElement(By.xpath('//input[@type="submit" and @name="saveAndCloseTicket" and @value="Save & Close"]')).click();
        await browser.sleep(5000);

        await waitForElement(By.xpath(`//div[@class="kanbanCardContent"]//h4/a[contains(., "${TASK_NAME}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD, true));

    it('can create project', createProject);
    it('can add task', addTask);
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id io.leantime.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login with username', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD, false));
    it('can create project', createProject);
    it('can add task', addTask);
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('check project and task exist', contentExists);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
